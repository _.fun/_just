(function() {
  const strokeTimeout = 300
  const strokeStopeKey = 'z'
  let isInLoop = false
  let keyboardCharacters = ['3', 'R']
  let keyboardIndex = 0

  document.addEventListener('keypress', (event) => {
    const keyName = event.key

    if (keyName != strokeStopeKey) return
    
    isInLoop = !isInLoop
    keyboardIndex = 0
  })

  pressKeyboardCharacters = function() {
    if (!isInLoop) return

    let currentKeystroke = keyboardCharacters[keyboardIndex]
    keyboardIndex++
    if (keyboardCharacters.length <= keyboardIndex) keyboardIndex = 0
    
    console.log(currentKeystroke)
    let event      = new Event("keydown")
    event.key      = currentKeystroke
    event.keyCode  = event.key.charCodeAt(0)
    event.which    = event.keyCode
    event.altKey   = false
    event.ctrlKey  = true
    event.shiftKey = false
    event.metaKey  = false
    event.bubbles  = true
    document.dispatchEvent(event)
  }

  setInterval(pressKeyboardCharacters, strokeTimeout)
})()
